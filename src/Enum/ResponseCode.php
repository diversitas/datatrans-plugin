<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Enum;

/**
 * Class ResponseCode
 * @package Diversitas\SyliusDatatransPlugin\Enum
 */
abstract class ResponseCode
{
    public const ACCESS_DENIED_BY_SIGN_CONTROL = 1007;
}