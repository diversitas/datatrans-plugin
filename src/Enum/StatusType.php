<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Enum;

/**
 * Class StatusType
 * @package Diversitas\SyliusDatatransPlugin\Enum
 */
abstract class StatusType
{
    const SUCCESS = 'success';
    const CANCEL = 'cancel';
    const ERROR = 'error';
}