<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Contracts;

use Payum\Core\ApiAwareInterface as PayumAwareInterface;

/**
 * Interface ApiAwareInterface
 * @package Diversitas\SyliusDatatransPlugin\Contracts
 */
interface ApiAwareInterface extends PayumAwareInterface
{

}
