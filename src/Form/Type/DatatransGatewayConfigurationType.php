<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class DatatransGatewayConfigurationType
 * @package Diversitas\SyliusDatatransPlugin\Form\Type
 */
final class DatatransGatewayConfigurationType extends AbstractType
{
    private $paymentOptions;

    /**
     * DatatransGatewayConfigurationType constructor.
     * @param array $paymentOptions
     */
    public function __construct(array $paymentOptions)
    {
        $this->paymentOptions = $paymentOptions ?? [];
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'merchantId',
                TextType::class,
                [
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.merchant_id',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'targetUrl',
                UrlType::class,
                [
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.target_url',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'hmacKey1',
                TextType::class,
                [
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.hmac_key_1',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'hmacKey2',
                TextType::class,
                [
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.hmac_key_2',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'paymentMethod',
                ChoiceType::class,
                [
                    'multiple'    => true,
                    'placeholder' => 'Choose an option',
                    'choices'     => $this->paymentOptions,
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.payment_method',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'theme',
                TextType::class,
                [
                    'label'       => 'diversitas_sylius_datatrans_plugin.ui.theme',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'diversitas_sylius_datatrans_plugin.merchant_id.not_blank',
                                'groups'  => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'sandbox',
                CheckboxType::class,
                [
                    'label' => 'diversitas_sylius_datatrans_plugin.ui.sandbox',
                ]
            );
    }
}
