<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Controller;

use Payum\Bundle\PayumBundle\Controller\PayumController;
use Payum\Core\Request\Notify;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\PaymentRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class WebHookController
 * @package Diversitas\SyliusDatatransPlugin\Controller
 */
class WebHookController extends PayumController
{
    private $paymentRepository;

    /**
     * WebHookController constructor.
     * @param PaymentRepository $paymentRepository
     */
    public function __construct(PaymentRepository $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function webHookAction(Request $request)
    {
        $gatewayName = $request->request->get('gateway_name');
        if (empty($gatewayName)) {
            throw new \InvalidArgumentException("Gateway name is not specified", 400);
        }

        $paymentId = $request->request->get('paymentId', null);
        if (is_null($paymentId)) {
            throw new \InvalidArgumentException("Payment ID wasn't specified in the request.", 400);
        }

        $payment = $this->paymentRepository->find($paymentId);
        if (empty($payment)) {
            throw new \Exception("Payment not found for ID# {$paymentId}", 404);
        }

        $gateway = $this->getPayum()->getGateway($gatewayName);
        $gateway->execute(new Notify($payment));

        return new Response('', 204);
    }
}
