<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Helpers;

/**
 * Class SignGenerator
 * @package Diversitas\SyliusDatatransPlugin\Helpers
 */
class SignGenerator
{
    /**
     * Generates HMAC-SHA256 signature and returns it as hexadecimal string
     *
     * @param string $hexaKey - merchant's hmac key obtained from web admin tool
     * @param int $merchantId - merchant's id
     * @param int $amount - amount in cents
     * @param string $currency - three-letter currency code
     * @param string $refno - reference number
     * @return string HMAC-SHA256 signature (lowercase)
     */
    public static function getHexaSHA256Signature(
        string $hexaKey,
        int $merchantId,
        int $amount,
        string $currency,
        string $refno
    ) {
        $valueToSign = sprintf("%s%s%s%s", $merchantId, $amount, $currency, $refno);

        return hash_hmac('sha256', $valueToSign, pack('H*', $hexaKey));
    }
}
