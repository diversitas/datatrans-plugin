<?php

namespace Diversitas\SyliusDatatransPlugin\Helpers;

/**
 * Class QueryStringHelper
 * @package Diversitas\SyliusDatatransPlugin\Helpers
 */
class QueryStringHelper
{
    /** @var array $parts */
    private $parts = [];

    /**
     * @param $key
     * @param $value
     */
    public function add($key, $value): void
    {
        $this->parts[] = [
            'key'   => $key,
            'value' => $value
        ];
    }

    /**
     * @param string $separator
     * @param string $equals
     * @return string
     */
    public function build($separator = '&', $equals = '=')
    {
        $queryString = [];
        foreach ($this->parts as $part) {
            $queryString[] = urlencode($part['key']) . $equals . urlencode($part['value']);
        }

        return implode($separator, $queryString);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->build();
    }
}
