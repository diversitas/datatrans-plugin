<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Client;

use Diversitas\SyliusDatatransPlugin\Helpers\Sign2Generator;

/**
 * Class DatatransClient
 * @package Diversitas\SyliusDatatransPlugin\Client
 */
class DatatransClient
{
    /** @var int $merchantId */
    private $merchantId;

    /** @var string $targetUrl */
    private $targetUrl;

    /** @var string $hmacKey1 */
    private $hmacKey1;

    /** @var string $hmacKey2 */
    private $hmacKey2;

    /** @var string $theme */
    private $theme;

    /** @var array $paymentMethods */
    private $paymentMethods = [];

    /**
     * DatatransClient constructor.
     * @param int $merchantId
     * @param string $targetUrl
     * @param string $hmacKey1
     * @param string $hmacKey2
     * @param string $theme
     * @param array $paymentMethods
     */
    public function __construct(
        int $merchantId,
        string $targetUrl,
        string $hmacKey1,
        string $hmacKey2,
        string $theme,
        array $paymentMethods
    ) {
        $this->merchantId = $merchantId;
        $this->targetUrl = $targetUrl;
        $this->hmacKey1 = $hmacKey1;
        $this->hmacKey2 = $hmacKey2;
        $this->theme = $theme;
        $this->paymentMethods = $paymentMethods;
    }

    /**
     * @return int
     */
    public function getMerchantId(): int
    {
        return $this->merchantId;
    }

    /**
     * @return string
     */
    public function getTargetUrl(): string
    {
        return $this->targetUrl;
    }

    /**
     * @return string
     */
    public function getHmacKey1(): string
    {
        return $this->hmacKey1;
    }

    /**
     * @return string
     */
    public function getHmacKey2(): string
    {
        return $this->hmacKey2;
    }

    /**
     * @return string
     */
    public function getTheme(): string
    {
        return $this->theme;
    }

    /**
     * @return array
     */
    public function getPaymentMethods(): array
    {
        return $this->paymentMethods;
    }

    /**
     * @param array $httpRequest
     * @return bool
     */
    public function validateResponse(array $httpRequest): bool
    {
        return $this->validateSign2Request($httpRequest);
    }

    /**
     * @param array $httpRequest
     * @return bool
     */
    private function validateSign2Request(array $httpRequest)
    {
        return $httpRequest['sign2'] === Sign2Generator::getHexaSHA256Signature(
                $this->getHmacKey2(),
                (int)$httpRequest['merchantId'],
                (int)$httpRequest['amount'],
                $httpRequest['currency'],
                $httpRequest['uppTransactionId']
            );
    }
}
