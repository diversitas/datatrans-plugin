<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Factory;

use Diversitas\SyliusDatatransPlugin\Client\DatatransClient;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;

/**
 * Class DatatransPaymentGatewayFactory
 * @package Diversitas\SyliusDatatransPlugin\Factory
 */
class DatatransPaymentGatewayFactory extends GatewayFactory
{
    public const FACTORY_NAME = 'datatrans';
    public const TITLE_NAME = 'Datatrans';

    /**
     * @param ArrayObject $config
     */
    protected function populateConfig(ArrayObject $config): void
    {
        $config->defaults(
            [
                'payum.factory_name'  => self::FACTORY_NAME,
                'payum.factory_title' => self::TITLE_NAME,
            ]
        );

        $config['payum.required_options'] = [
            'merchantId',
            'targetUrl',
            'hmacKey1',
            'hmacKey2',
            'paymentMethod',
            'theme',
        ];

        $config['payum.api'] = function (ArrayObject $config) {
            $config->validateNotEmpty($config['payum.required_options']);

            return new DatatransClient(
                (int)$config['merchantId'],
                $config['targetUrl'],
                $config['hmacKey1'],
                $config['hmacKey2'],
                $config['theme'],
                $config['paymentMethod']
            );
        };
    }
}
