<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Sync;

/**
 * Class SyncAction
 * @package Diversitas\SyliusDatatransPlugin\Action
 */
class SyncAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;

    /**
     * @inheritDoc
     * @param Sync $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);
        $model = ArrayObject::ensureArrayObject($request->getModel());

        $this->gateway->execute($httpRequest = new GetHttpRequest());
        $model->replace($httpRequest->request);
    }

    /**
     * @inheritDoc
     */
    public function supports($request)
    {
        return $request instanceof Sync && $request->getModel() instanceof \ArrayAccess;
    }
}
