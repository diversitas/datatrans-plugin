<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Action;

use Diversitas\SyliusDatatransPlugin\Contracts\ApiAwareInterface;
use Diversitas\SyliusDatatransPlugin\Traits\ApiAwareTrait;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Notify;
use Payum\Core\Request\Sync;

/**
 * Class NotifyAction
 * @package Diversitas\SyliusDatatransPlugin\Action
 */
class NotifyAction implements ActionInterface, ApiAwareInterface, GatewayAwareInterface
{
    use ApiAwareTrait, GatewayAwareTrait;

    /**
     * @inheritDoc
     * @param Notify $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);
        $details = ArrayObject::ensureArrayObject($request->getModel());

        $this->gateway->execute($httpRequest = new GetHttpRequest());
        $webHookRequest = $httpRequest->request;

        if (isset($webHookRequest['sign2'])) {
            if (!$this->datatransApiClient->validateResponse($webHookRequest)) {
                throw new HttpResponse('The request data is malformed.', 403);
            }
        }

        $this->gateway->execute(new Sync($details));
    }

    /**
     * @inheritDoc
     */
    public function supports($request)
    {
        return $request instanceof Notify && $request->getModel() instanceof \ArrayAccess;
    }
}
