<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Action;

use Diversitas\SyliusDatatransPlugin\Enum\StatusType;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\GetStatusInterface;

/**
 * Class StatusAction
 * @package Diversitas\SyliusDatatransPlugin\Action
 */
class StatusAction implements ActionInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;

    /**
     * @param GetStatusInterface $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);
        $model = ArrayObject::ensureArrayObject($request->getModel());

        if (!isset($model['status'])) {
            $request->markNew();

            return;
        }

        if (isset($model['sign2']) && $model['status'] === StatusType::SUCCESS) {
            $request->markCaptured();

            return;
        }

        if (isset($model['status']) && $model['status'] === StatusType::CANCEL) {
            $request->markCanceled();

            return;
        }

        if (isset($model['status']) && isset($model['errorCode']) && $model['status'] === StatusType::ERROR) {
            $request->markFailed();

            return;
        }

        $request->markUnknown();
    }

    /**
     * @inheritDoc
     */
    public function supports($request)
    {
        return
            $request instanceof GetStatusInterface &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
