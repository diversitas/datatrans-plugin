<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin\Action;

use Diversitas\SyliusDatatransPlugin\Contracts\ApiAwareInterface;
use Diversitas\SyliusDatatransPlugin\Enum\ResponseCode;
use Diversitas\SyliusDatatransPlugin\Enum\StatusType;
use Diversitas\SyliusDatatransPlugin\Helpers\QueryStringHelper;
use Diversitas\SyliusDatatransPlugin\Helpers\Sign2Generator;
use Diversitas\SyliusDatatransPlugin\Helpers\SignGenerator;
use Diversitas\SyliusDatatransPlugin\Traits\ApiAwareTrait;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHttpRequest;

/**
 * Class CaptureAction
 * @package Diversitas\SyliusDatatransPlugin\Action
 */
class CaptureAction implements ActionInterface, ApiAwareInterface, GatewayAwareInterface
{
    use ApiAwareTrait, GatewayAwareTrait;

    /**
     * @inheritDoc
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);
        $details = ArrayObject::ensureArrayObject($request->getModel());

        $details->validatedKeysSet(
            [
                'amount',
                'refno',
                'currency_code',
            ]
        );

        $this->gateway->execute($httpRequest = new GetHttpRequest());
        $datatransRequest = $httpRequest->request;

        if (isset($datatransRequest['status']) && $datatransRequest['status'] === StatusType::CANCEL) {
            $details['status'] = $datatransRequest['status'];

            return;
        }

        // user is coming back from the Datatrans side. return him to done action.
        if (isset($datatransRequest['sign2']) && $datatransRequest['status'] === StatusType::SUCCESS) {
            // if sign2 was malformed we mark response as failed
            if ($datatransRequest['sign2'] !== $this->generateSign2($datatransRequest)) {
                $details['errorCode'] = ResponseCode::ACCESS_DENIED_BY_SIGN_CONTROL;
                $details['status'] = StatusType::ERROR;

                return;
            }

            $details['sign2'] = $datatransRequest['sign2'];
            $details['status'] = $datatransRequest['status'];

            return;
        }

        if (isset($datatransRequest['errorCode']) && $datatransRequest['status'] === StatusType::ERROR) {
            $details['errorCode'] = $datatransRequest['errorCode'];
            $details['status'] = $datatransRequest['status'];

            return;
        }

        $url = $this->datatransApiClient->getTargetUrl();
        $query = $this->prepareQuery($request, $details);

        throw new HttpRedirect(sprintf("%s?%s", $url, $query), 302, ['application/x-www-form-urlencoded']);
    }

    /**
     * @inheritDoc
     */
    public function supports($request)
    {
        return $request instanceof Capture && $request->getModel() instanceof \ArrayAccess;
    }

    /**
     * @param Capture $request
     * @param ArrayObject $details
     * @return string
     */
    private function prepareQuery($request, $details): string
    {
        $merchantId = $this->datatransApiClient->getMerchantId();
        $refno = $details['refno'];
        $amount = $details['amount'];
        $currency = $details['currency_code'];
        $hmacKey1 = $this->datatransApiClient->getHmacKey1();

        $parts = [
            'merchantId'   => $merchantId,
            'refno'        => $refno,
            'amount'       => $amount,
            'currency'     => $currency,
            'theme'        => $this->datatransApiClient->getTheme(),
            'sign'         => SignGenerator::getHexaSHA256Signature(
                $hmacKey1,
                $merchantId,
                $amount,
                $currency,
                $refno
            ),
            'successUrl'   => $request->getToken()->getTargetUrl(),
            'cancelUrl'    => $request->getToken()->getTargetUrl(),
            'errorUrl'     => $request->getToken()->getTargetUrl(),
            'paymentId'    => $request->getFirstModel()->getId(),
            'gateway_name' => $request->getToken()->getGatewayName(),
        ];

        $queryString = new QueryStringHelper();
        foreach ($parts as $key => $value) {
            $queryString->add($key, $value);
        }

        $paymentMethods = $this->datatransApiClient->getPaymentMethods();
        foreach ($paymentMethods as $ley => $paymentMethod) {
            $queryString->add('paymentmethod', $paymentMethod);
        }

        return $queryString->build();
    }

    /**
     * @param array $datatransRequest
     * @return string
     */
    private function generateSign2(array $datatransRequest)
    {
        return Sign2Generator::getHexaSHA256Signature(
            $this->datatransApiClient->getHmacKey2(),
            (int)$datatransRequest['merchantId'],
            (int)$datatransRequest['amount'],
            $datatransRequest['currency'],
            $datatransRequest['uppTransactionId']
        );
    }
}
