<?php


namespace Diversitas\SyliusDatatransPlugin\Traits;

use Diversitas\SyliusDatatransPlugin\Client\DatatransClient;
use Payum\Core\Exception\UnsupportedApiException;

/**
 * Trait ApiAwareTrait
 * @package Diversitas\SyliusDatatransPlugin\Traits
 */
trait ApiAwareTrait
{
    /** @var DatatransClient $datatransApiClient */
    protected $datatransApiClient;

    /**
     * @inheritDoc
     */
    public function setApi($datatransApiClient)
    {
        if (!$datatransApiClient instanceof DatatransClient) {
            throw new UnsupportedApiException('Not supported. Expected an instance of ' . DatatransClient::class);
        }

        $this->datatransApiClient = $datatransApiClient;
    }
}
