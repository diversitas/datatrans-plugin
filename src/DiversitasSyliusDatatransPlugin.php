<?php

declare(strict_types=1);

namespace Diversitas\SyliusDatatransPlugin;

use Sylius\Bundle\CoreBundle\Application\SyliusPluginTrait;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class DatatransPlugin
 * @package Diversitas\SyliusDatatransPlugin
 */
final class DiversitasSyliusDatatransPlugin extends Bundle
{
    use SyliusPluginTrait;
}
