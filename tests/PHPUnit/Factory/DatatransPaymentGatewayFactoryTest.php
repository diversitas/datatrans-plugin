<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Factory;

use Diversitas\SyliusDatatransPlugin\Factory\DatatransPaymentGatewayFactory;
use Payum\Core\Exception\LogicException;
use Payum\Core\Gateway;
use Payum\Core\GatewayFactory;
use Payum\Core\GatewayFactoryInterface;
use PHPUnit\Framework\TestCase;

class DatatransPaymentGatewayFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldSubClassGatewayFactory()
    {
        $reflectionClass = new \ReflectionClass(DatatransPaymentGatewayFactory::class);

        $this->assertTrue($reflectionClass->isSubclassOf(GatewayFactory::class));
    }

    /**
     * @test
     */
    public function shouldCreateCoreGatewayFactoryIfNotPassed()
    {
        $factory = new DatatransPaymentGatewayFactory();

        $this->assertObjectHasAttribute('coreGatewayFactory', $factory);
    }

    /**
     * @test
     */
    public function shouldUseCoreGatewayFactoryPassedAsSecondArgument()
    {
        $coreGatewayFactory = $this->createMock(GatewayFactoryInterface::class);
        $factory = new DatatransPaymentGatewayFactory([], $coreGatewayFactory);

        $this->assertObjectHasAttribute('coreGatewayFactory', $factory);
    }

    /**
     * @test
     */
    public function shouldAllowCreateGateway()
    {
        $factory = new DatatransPaymentGatewayFactory();
        $gateway = $factory->create(
            [
                'merchantId'    => 1100020093,
                'targetUrl'     => 'https://pay.sandbox.datatrans.com/upp/jsp/upStart.jsp',
                'hmacKey1'      => '44761f6fa546af6ebcbd74da0ab714680f39a8aaf158ccc46ec97c94f66d2370aee9e211deef6a518d5eff0f62a3ac99c959631cb4edd99b9fc4b580f724f4af',
                'hmacKey2'      => 'f68b6b98bcd60e3c2d097c8cd4989a6f1d3781b3a26c680e97aaefbc621878fb878495caf9ac6e047e656a5cad7caa0ab4ec39900e2a4a0a3da477f83be6c459',
                'paymentMethod' => ['VIS'],
                'theme'         => 'theme'
            ]
        );

        $this->assertInstanceOf(Gateway::class, $gateway);
        $this->assertNotEmpty($gateway, 'apis');
        $this->assertNotEmpty($gateway, 'actions');
        $this->assertNotEmpty($gateway, 'extensions');
    }

    /**
     * @test
     */
    public function shouldConfigContainFactoryNameAndTitle()
    {
        $factory = new DatatransPaymentGatewayFactory();

        $config = $factory->createConfig();
        $this->assertIsArray($config, 'array');

        $this->assertArrayHasKey('payum.factory_name', $config);
        $this->assertEquals(DatatransPaymentGatewayFactory::FACTORY_NAME, $config['payum.factory_name']);

        $this->assertArrayHasKey('payum.factory_title', $config);
        $this->assertEquals(DatatransPaymentGatewayFactory::TITLE_NAME, $config['payum.factory_title']);
    }

    /**
     * @test
     */
    public function shouldAllowCreateGatewayConfig()
    {
        $factory = new DatatransPaymentGatewayFactory();
        $config = $factory->createConfig();
        $this->assertIsArray($config, 'array');
        $this->assertNotEmpty($config);
    }

    /**
     * @test
     */
    public function shouldThrowIfRequiredOptionsNotPassed()
    {
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage(
            'The merchantId, targetUrl, hmacKey1, hmacKey2, paymentMethod, theme fields are required.'
        );

        $factory = new DatatransPaymentGatewayFactory();
        $factory->create();
    }
}
