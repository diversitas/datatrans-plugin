<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Controller;

use Diversitas\SyliusDatatransPlugin\Controller\WebHookController;
use Payum\Core\GatewayInterface;
use Payum\Core\Payum;
use Payum\Core\Registry\RegistryInterface;
use Payum\Core\Request\Notify;
use Payum\Core\Security\GenericTokenFactoryInterface;
use Payum\Core\Security\HttpRequestVerifierInterface;
use Payum\Core\Storage\StorageInterface;
use PHPUnit\Framework\TestCase;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\PaymentRepository;
use Sylius\Component\Core\Model\Payment;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class WebHookControllerTest extends TestCase
{
    /**
     * @test
     * @throws \Exception
     */
    public function testWebHookCalledSuccessfully()
    {
        $paymentRepositoryMock = $this
            ->getMockBuilder(PaymentRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $request = new Request(
            [],
            $this->mockedSuccessResponse()
        );
        $request->setMethod('POST');

        $httpRequestVerifierMock = $this->createMock(HttpRequestVerifierInterface::class);

        $gatewayMock = $this->createMock(GatewayInterface::class);
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(Notify::class));

        $registryMock = $this->createMock(RegistryInterface::class);
        $registryMock
            ->expects($this->once())
            ->method('getGateway')
            ->will($this->returnValue($gatewayMock));

        $payum = new Payum(
            $registryMock,
            $httpRequestVerifierMock,
            $this->createMock(GenericTokenFactoryInterface::class),
            $this->createMock(StorageInterface::class)
        );

        $container = new Container;
        $container->set('payum', $payum);

        $payment = new Payment();
        $this->setProtectedProperty($payment, 'id', 1);

        $paymentRepositoryMock
            ->expects($this->once())
            ->method('find')
            ->willReturn($payment);

        $controller = new WebHookController($paymentRepositoryMock);
        $controller->setContainer($container);
        $response = $controller->webHookAction($request);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(204, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function testWebHookThrowsExceptionIfNoGateWaySpecified()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Gateway name is not specified");
        $this->expectExceptionCode(400);

        $paymentRepositoryMock = $this
            ->getMockBuilder(PaymentRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $request = new Request(
            [],
            []
        );
        $request->setMethod('POST');

        $controller = new WebHookController($paymentRepositoryMock);
        $controller->webHookAction($request);
    }

    /**
     * @test
     */
    public function testWebHookThrowsExceptionIfNoPaymentIdSpecified()
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage("Payment ID wasn't specified in the request.");
        $this->expectExceptionCode(400);

        $paymentRepositoryMock = $this
            ->getMockBuilder(PaymentRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $request = new Request(
            [],
            ['gateway_name' => 'datatrans_visa',]
        );
        $request->setMethod('POST');

        $controller = new WebHookController($paymentRepositoryMock);
        $controller->webHookAction($request);
    }

    /**
     * @test
     */
    public function testWebHookThrowsExceptionIfNoPaymentFound()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage("Payment not found for ID# 1");
        $this->expectExceptionCode(404);

        $paymentRepositoryMock = $this
            ->getMockBuilder(PaymentRepository::class)
            ->onlyMethods(['find'])
            ->disableOriginalConstructor()
            ->getMock();

        $request = new Request(
            [],
            [
                'gateway_name' => 'datatrans_visa',
                'paymentId'    => '1',
            ]
        );
        $request->setMethod('POST');

        $controller = new WebHookController($paymentRepositoryMock);
        $controller->webHookAction($request);
    }

    /**
     * @param $object
     * @param $property
     * @param $value
     * @throws \ReflectionException
     */
    private function setProtectedProperty($object, $property, $value): void
    {
        $reflection = new \ReflectionClass($object);
        $reflection_property = $reflection->getProperty($property);
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($object, $value);
    }

    /**
     * @return array
     */
    private function mockedSuccessResponse(): array
    {
        return [
            'expm'                  => '12',
            'expy'                  => '21',
            'sign'                  => '8a6e0c7497c268d35b15f29004127bde427edb4e68534b8b490d9758c4e0bdfd',
            'refno'                 => '000000063',
            'sign2'                 => 'a5b787e4ab28435a3471f7da003078b60912efbf5fcd89b2050b065adc7d9479',
            'theme'                 => 'DT2015',
            'amount'                => '16500',
            'cardno'                => '490000xxxxxx0086',
            'status'                => 'success',
            'pmethod'               => 'VIS',
            'reqtype'               => 'CAA',
            'currency'              => 'CHF',
            'testOnly'              => 'yes',
            'paymentId'             => '1',
            'merchantId'            => '1100020093',
            'uppMsgType'            => 'post',
            'gateway_name'          => 'datatrans_visa',
            'responseCode'          => '01',
            'currency_code'         => 'CHF',
            'responseMessage'       => 'Authorized',
            'uppTransactionId'      => '200326120742616710',
            'authorizationCode'     => '756696816',
            'acqAuthorizationCode'  => '120756',
            'returnCustomerCountry' => 'USA',
        ];
    }
}