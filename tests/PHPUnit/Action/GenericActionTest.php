<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Client\DatatransClient;
use Payum\Core\Action\ActionInterface;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayInterface;
use Payum\Core\Request\Generic;
use Payum\Core\Security\TokenInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

abstract class GenericActionTest extends TestCase
{
    /**
     * @var Generic
     */
    protected $requestClass;

    /**
     * @var string
     */
    protected $actionClass;

    /**
     * @var ActionInterface
     */
    protected $action;

    protected function setUp(): void
    {
        $this->action = new $this->actionClass();
    }

    /**
     * @return array
     */
    public function provideSupportedRequests()
    {
        return [
            [new $this->requestClass([])],
            [new $this->requestClass(new \ArrayObject())],
        ];
    }

    /**
     * @return array
     */
    public function provideNotSupportedRequests()
    {
        return [
            ['foo'],
            [['foo']],
            [new \stdClass()],
            [new $this->requestClass('foo')],
            [new $this->requestClass(new \stdClass())],
            [$this->getMockForAbstractClass(Generic::class, [[]])],
        ];
    }

    /**
     * @test
     * @throws \ReflectionException
     */
    public function shouldImplementActionInterface()
    {
        $reflectionClass = new \ReflectionClass($this->actionClass);

        $this->assertTrue($reflectionClass->implementsInterface(ActionInterface::class));
    }

    /**
     * @test
     *
     * @dataProvider provideSupportedRequests
     * @param $request
     */
    public function shouldSupportRequest($request)
    {
        $this->assertTrue($this->action->supports($request));
    }

    /**
     * @test
     *
     * @dataProvider provideNotSupportedRequests
     * @param $request
     */
    public function shouldNotSupportRequest($request)
    {
        $this->assertFalse($this->action->supports($request));
    }

    /**
     * @test
     *
     * @dataProvider provideNotSupportedRequests
     * @param $request
     */
    public function throwIfNotSupportedRequestGivenAsArgumentForExecute($request)
    {
        $this->expectException(RequestNotSupportedException::class);
        $this->action->execute($request);
    }

    /**
     * @return MockObject|GatewayInterface
     */
    protected function createGatewayMock()
    {
        return $this->createMock(GatewayInterface::class);
    }

    /**
     * @return MockObject|TokenInterface
     */
    protected function createTokenMock()
    {
        return $this->createMock(TokenInterface::class);
    }

    /**
     * @return MockObject|DatatransClient
     */
    protected function createApiMock()
    {
        return $this
            ->getMockBuilder(DatatransClient::class)
            ->disableOriginalConstructor()
            ->onlyMethods([])
            ->getMock();
    }

    /**
     * @param array $args
     * @return MockObject
     */
    protected function createApiMockWithArgs(array $args)
    {
        return $this
            ->getMockBuilder(DatatransClient::class)
            ->onlyMethods([])
            ->setConstructorArgs($args)
            ->getMock();
    }

    /**
     * @param $object
     * @param $property
     * @param $value
     * @throws \ReflectionException
     */
    protected function setProtectedProperty($object, $property, $value): void
    {
        $reflection = new \ReflectionClass($object);
        $reflection_property = $reflection->getProperty($property);
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($object, $value);
    }

    /**
     * @return array
     */
    protected function datatransClientArgs(): array
    {
        return [
            '1100020093',
            'https://pay.sandbox.datatrans.com/upp/jsp/upStart.jsp',
            '44761f6fa546af6ebcbd74da0ab714680f39a8aaf158ccc46ec97c94f66d2370aee9e211deef6a518d5eff0f62a3ac99c959631cb4edd99b9fc4b580f724f4af',
            'f68b6b98bcd60e3c2d097c8cd4989a6f1d3781b3a26c680e97aaefbc621878fb878495caf9ac6e047e656a5cad7caa0ab4ec39900e2a4a0a3da477f83be6c459',
            'DT2015',
            ['VIS'],
        ];
    }
}
