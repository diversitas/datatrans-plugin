<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Action\CaptureAction;
use Diversitas\SyliusDatatransPlugin\Action\SyncAction;
use Diversitas\SyliusDatatransPlugin\Enum\StatusType;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Sync;

class SyncActionTest extends GenericActionTest
{
    protected $actionClass = SyncAction::class;
    protected $requestClass = Sync::class;

    /**
     * @test
     */
    public function shouldImplementGatewayAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(GatewayAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldSupportSyncWithArrayAsModel()
    {
        $action = new SyncAction();

        $this->assertTrue($action->supports(new Sync([])));
    }

    /**
     * @test
     */
    public function shouldNotSupportAnythingNotSync()
    {
        $action = new SyncAction();

        $this->assertFalse($action->supports(new \stdClass()));
    }

    /**
     * @test
     */
    public function shouldNotSupportSyncWithNotArrayAccessModel()
    {
        $action = new SyncAction();

        $this->assertFalse($action->supports(new Sync(new \stdClass())));
    }

    /**
     * @test
     */
    public function throwIfNotSupportedRequestGivenAsArgumentOnExecute()
    {
        $this->expectException(RequestNotSupportedException::class);
        $action = new SyncAction();

        $action->execute(new \stdClass());
    }

    /**
     * @test
     */
    public function shouldGetHttpRequestAndUpdateModel()
    {
        $datatransMockedResponse = $this->mockedDatatransResponse();

        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $action = new SyncAction();
        $action->setGateway($gatewayMock);
        $action->execute($request = new Sync([]));

        $model = iterator_to_array($request->getModel());

        $this->assertArrayHasKey('merchantId', $model);
        $this->assertEquals(1100020093, $model['merchantId']);

        $this->assertArrayHasKey('sign2', $model);
        $this->assertEquals("2484dc6b19d8e84ffdae3b2783dd27d8902f01cd325dc06d3e2251aa01aec37f", $model['sign2']);

        $this->assertArrayHasKey('status', $model);
        $this->assertEquals(StatusType::SUCCESS, $model['status']);
    }

    /**
     * @return array
     */
    private function mockedDatatransResponse(): array
    {
        return [
            'merchantId'        => 1100020093,
            'status'            => StatusType::SUCCESS,
            'authorizationCode' => 123456789,
            'sign2'             => '2484dc6b19d8e84ffdae3b2783dd27d8902f01cd325dc06d3e2251aa01aec37f',
            'amount'            => 4567,
            'uppTransactionId'  => "200316101858862155",
            'currency'          => 'CHF',
        ];
    }
}