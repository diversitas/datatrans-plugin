<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Action\CaptureAction;
use Diversitas\SyliusDatatransPlugin\Action\NotifyAction;
use Diversitas\SyliusDatatransPlugin\Contracts\ApiAwareInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Notify;
use Payum\Core\Request\Sync;

class NotifyActionTest extends GenericActionTest
{
    protected $actionClass = NotifyAction::class;
    protected $requestClass = Notify::class;

    /**
     * @test
     */
    public function shouldImplementApiAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(ApiAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldImplementGatewayAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(GatewayAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldExecuteSyncWithSuccessResponse()
    {
        $gatewayMock = $this->createGatewayMock();

        $datatransMockedResponse = $this->mockedSuccessResponse();
        $gatewayMock
            ->expects($this->at(0))
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $gatewayMock
            ->expects($this->at(1))
            ->method('execute')
            ->with($this->isInstanceOf(Sync::class));

        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $action = new NotifyAction();
        $action->setApi($apiMock);
        $action->setGateway($gatewayMock);

        $action->execute($request = new Notify([]));
    }

    /**
     * @test
     */
    public function shouldExecuteSyncWithErrorResponse()
    {
        $gatewayMock = $this->createGatewayMock();

        $datatransMockedResponse = $this->mockedFailureResponse();
        $gatewayMock
            ->expects($this->at(0))
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $gatewayMock
            ->expects($this->at(1))
            ->method('execute')
            ->with($this->isInstanceOf(Sync::class));

        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $action = new NotifyAction();
        $action->setApi($apiMock);
        $action->setGateway($gatewayMock);

        $action->execute(new Notify([]));
    }

    /**
     * @test
     */
    public function shouldThrowExceptionIfSign2WasMalformed()
    {
        $this->expectException(HttpResponse::class);

        $gatewayMock = $this->createGatewayMock();

        $datatransMockedResponse = $this->mockedMalformedSuccessResponse();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());
        $action = new NotifyAction();
        $action->setApi($apiMock);
        $action->setGateway($gatewayMock);

        $action->execute(new Notify([]));
    }

    /**
     * @return array
     */
    private function mockedSuccessResponse(): array
    {
        return [
            'expm'                  => '12',
            'expy'                  => '21',
            'sign'                  => '8a6e0c7497c268d35b15f29004127bde427edb4e68534b8b490d9758c4e0bdfd',
            'refno'                 => '000000063',
            'sign2'                 => 'a5b787e4ab28435a3471f7da003078b60912efbf5fcd89b2050b065adc7d9479',
            'theme'                 => 'DT2015',
            'amount'                => '16500',
            'cardno'                => '490000xxxxxx0086',
            'status'                => 'success',
            'pmethod'               => 'VIS',
            'reqtype'               => 'CAA',
            'currency'              => 'CHF',
            'testOnly'              => 'yes',
            'paymentId'             => '82',
            'merchantId'            => '1100020093',
            'uppMsgType'            => 'post',
            'gateway_name'          => 'datatrans_visa',
            'responseCode'          => '01',
            'currency_code'         => 'CHF',
            'responseMessage'       => 'Authorized',
            'uppTransactionId'      => '200326120742616710',
            'authorizationCode'     => '756696816',
            'acqAuthorizationCode'  => '120756',
            'returnCustomerCountry' => 'USA',
        ];
    }

    /**
     * @return array
     */
    private function mockedFailureResponse(): array
    {
        return [
            'expm'                  => '12',
            'expy'                  => '21',
            'sign'                  => '8a6e0c7497c268d35b15f29004127bde427edb4e68534b8b490d9758c4e0bdfd',
            'refno'                 => '000000063',
            'theme'                 => 'DT2015',
            'amount'                => '16500',
            'status'                => 'error',
            'pmethod'               => 'VIS',
            'reqtype'               => 'CAA',
            'currency'              => 'CHF',
            'testOnly'              => 'yes',
            'errorCode'             => '1404',
            'paymentId'             => '81',
            'merchantId'            => '1100020093',
            'uppMsgType'            => 'post',
            'errorDetail'           => 'Declined - card blocked',
            'acqErrorCode'          => '42',
            'errorMessage'          => 'card blocked',
            'gateway_name'          => 'datatrans_visa',
            'currency_code'         => 'CHF',
            'uppTransactionId'      => '200326120645656569',
            'returnCustomerCountry' => 'GBR',
        ];
    }

    /**
     * @return array
     */
    private function mockedMalformedSuccessResponse(): array
    {
        return [
            'expm'                  => '12',
            'expy'                  => '21',
            'sign'                  => '8a6e0c7497c268d35b15f29004127bde427edb4e68534b8b490d9758c4e0bdfd',
            'refno'                 => '000000063',
            'sign2'                 => '2484dc6b19d8e84ffdae3b2783dd27d8902f01cd325dc06d3e2251aa01aec37f',
            'theme'                 => 'DT2015',
            'amount'                => '16500',
            'cardno'                => '490000xxxxxx0086',
            'status'                => 'success',
            'pmethod'               => 'VIS',
            'reqtype'               => 'CAA',
            'currency'              => 'CHF',
            'testOnly'              => 'yes',
            'paymentId'             => '82',
            'merchantId'            => '1100020093',
            'uppMsgType'            => 'post',
            'gateway_name'          => 'datatrans_visa',
            'responseCode'          => '01',
            'currency_code'         => 'CHF',
            'responseMessage'       => 'Authorized',
            'uppTransactionId'      => '200326120742616710',
            'authorizationCode'     => '756696816',
            'acqAuthorizationCode'  => '120756',
            'returnCustomerCountry' => 'USA',
        ];
    }
}