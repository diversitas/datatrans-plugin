<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Action\CaptureAction;
use Diversitas\SyliusDatatransPlugin\Contracts\ApiAwareInterface;
use Diversitas\SyliusDatatransPlugin\Enum\ResponseCode;
use Diversitas\SyliusDatatransPlugin\Enum\StatusType;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\LogicException;
use Payum\Core\Exception\UnsupportedApiException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Model\Token;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHttpRequest;
use Sylius\Component\Core\Model\Payment;
use Sylius\Component\Payment\Model\PaymentInterface;

class CaptureActionTest extends GenericActionTest
{
    protected $actionClass = CaptureAction::class;
    protected $requestClass = Capture::class;

    /**
     * @test
     */
    public function shouldImplementApiAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(ApiAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldImplementGatewayAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(GatewayAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldAllowSetApi()
    {
        $expectedApi = $this->createApiMock([]);
        $action = new CaptureAction();
        $action->setApi($expectedApi);

        $this->assertObjectHasAttribute('datatransApiClient', $action);
    }

    /**
     * @test
     */
    public function throwIfUnsupportedApiGiven()
    {
        $this->expectException(UnsupportedApiException::class);

        $action = new CaptureAction();
        $action->setApi(new \stdClass);
    }

    /**
     * @test
     */
    public function shouldThrowIfAmountIsNotSet()
    {
        $model = [];
        $details = ArrayObject::ensureArrayObject($model);

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('The amount fields is not set.');

        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->never())
            ->method('execute')
            ->with($details)
            ->will($this->throwException(new LogicException()));

        $action = new CaptureAction();
        $action->setApi($this->createApiMock());
        $request = new Capture($model);
        $action->execute($request);
    }

    /**
     * @test
     */
    public function shouldThrowIfOrderNumberIsNotSet()
    {
        $model = [
            'amount' => 1000
        ];

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('The refno fields is not set');

        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->never())
            ->method('execute')
            ->will($this->throwException(new LogicException()));

        $action = new CaptureAction();
        $action->setApi($this->createApiMock());
        $request = new Capture($model);
        $action->execute($request);
    }

    /**
     * @test
     */
    public function shouldThrowIfCurrencyCodeIsNotSet()
    {
        $model = [
            'amount' => 1000,
            'refno'  => '00000099'
        ];

        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('The currency_code fields is not set');

        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->never())
            ->method('execute')
            ->will($this->throwException(new LogicException()));

        $action = new CaptureAction();
        $action->setApi($this->createApiMock());
        $request = new Capture($model);
        $action->execute($request);
    }

    /**
     * @test
     */
    public function shouldRedirectToDatatransSite()
    {
        $this->expectException(HttpRedirect::class);
        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $token = new Token();
        $token->setTargetUrl('theTargetUrl');
        $token->setGatewayName('datatrans_visa');
        $token->setDetails([]);

        $details = [
            'refno'         => '000000001',
            'amount'        => 1000,
            'currency_code' => 'CHF',
        ];

        $payment = new Payment();
        $this->setProtectedProperty($payment, 'id', 1);
        $payment->setDetails($details);


        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class));

        $action = new CaptureAction();
        $action->setGateway($gatewayMock);
        $action->setApi($apiMock);
        $request = new Capture($token);
        $request->setModel($payment);
        $request->setModel($details);

        $action->execute($request);
    }

    /**
     * @test
     */
    public function checkSuccessStatusTest()
    {
        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $token = new Token();
        $token->setTargetUrl('theTargetUrl');
        $token->setDetails([]);

        $details = [
            'refno'         => '000000001',
            'amount'        => 4567,
            'currency_code' => 'CHF',
        ];

        $datatransMockedResponse = $this->mockedDatatransSuccessResponse();
        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $action = new CaptureAction();
        $action->setGateway($gatewayMock);
        $action->setApi($apiMock);
        $request = new Capture($token);
        $request->setModel($details);

        $action->execute($request);

        $model = iterator_to_array($request->getModel());
        $this->assertArrayHasKey('sign2', $model);
        $this->assertEquals("a5b787e4ab28435a3471f7da003078b60912efbf5fcd89b2050b065adc7d9479", $model['sign2']);

        $this->assertArrayHasKey('status', $model);
        $this->assertEquals(StatusType::SUCCESS, $model['status']);
    }

    /**
     * @test
     */
    public function checkSuccessStatusSign2FailedTest()
    {
        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $token = new Token();
        $token->setTargetUrl('theTargetUrl');
        $token->setDetails([]);

        $details = [
            'refno'         => '000000001',
            'amount'        => 4567,
            'currency_code' => 'CHF',
        ];

        $payment = new Payment();
        $payment->setState(PaymentInterface::STATE_FAILED);

        $datatransMockedResponse = $this->mockedDatatransSuccessSign2FailedResponse();
        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $action = new CaptureAction();
        $action->setGateway($gatewayMock);
        $action->setApi($apiMock);
        $request = new Capture($token);
        $request->setModel($payment);
        $request->setModel($details);

        $action->execute($request);

        $model = iterator_to_array($request->getModel());
        $this->assertArrayHasKey('status', $model);
        $this->assertEquals(StatusType::ERROR, $model['status']);

        $this->assertArrayHasKey('errorCode', $model);
        $this->assertEquals(ResponseCode::ACCESS_DENIED_BY_SIGN_CONTROL, $model['errorCode']);

        $this->assertEquals(PaymentInterface::STATE_FAILED, $request->getFirstModel()->getState());
    }

    /**
     * @test
     */
    public function checkCancelStatusTest()
    {
        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $token = new Token();
        $token->setTargetUrl('theTargetUrl');
        $token->setDetails([]);

        $details = [
            'refno'         => '000000001',
            'amount'        => 4567,
            'currency_code' => 'CHF',
        ];

        $datatransMockedResponse = $this->mockedDatatransCancelResponse();
        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $action = new CaptureAction();
        $action->setGateway($gatewayMock);
        $action->setApi($apiMock);
        $request = new Capture($token);
        $request->setModel($details);

        $action->execute($request);

        $model = iterator_to_array($request->getModel());
        $this->assertArrayHasKey('status', $model);
        $this->assertEquals(StatusType::CANCEL, $model['status']);
    }

    /**
     * @test
     */
    public function checkErrorStatusTest()
    {
        $apiMock = $this->createApiMockWithArgs($this->datatransClientArgs());

        $token = new Token();
        $token->setTargetUrl('theTargetUrl');
        $token->setDetails([]);

        $details = [
            'refno'         => '000000001',
            'amount'        => 4567,
            'currency_code' => 'CHF',
        ];

        $datatransMockedResponse = $this->mockedDatatransErrorResponse();
        $gatewayMock = $this->createGatewayMock();
        $gatewayMock
            ->expects($this->once())
            ->method('execute')
            ->with($this->isInstanceOf(GetHttpRequest::class))
            ->will(
                $this->returnCallback(
                    function (GetHttpRequest $request) use ($datatransMockedResponse) {
                        $request->request = $datatransMockedResponse;
                    }
                )
            );

        $action = new CaptureAction();
        $action->setGateway($gatewayMock);
        $action->setApi($apiMock);
        $request = new Capture($token);
        $request->setModel($details);

        $action->execute($request);

        $model = iterator_to_array($request->getModel());
        $this->assertArrayHasKey('status', $model);
        $this->assertEquals(StatusType::ERROR, $model['status']);

        $this->assertArrayHasKey('errorCode', $model);
        $this->assertEquals(4, $model['errorCode']);
    }

    /**
     * @return array
     */
    private function mockedDatatransSuccessResponse(): array
    {
        return [
            'merchantId'        => 1100020093,
            'status'            => StatusType::SUCCESS,
            'authorizationCode' => 756696816,
            'sign2'             => 'a5b787e4ab28435a3471f7da003078b60912efbf5fcd89b2050b065adc7d9479',
            'amount'            => 16500,
            'uppTransactionId'  => "200326120742616710",
            'currency'          => 'CHF',
        ];
    }

    /**
     * @return array
     */
    private function mockedDatatransSuccessSign2FailedResponse(): array
    {
        return [
            'merchantId'        => 1100020093,
            'status'            => StatusType::SUCCESS,
            'authorizationCode' => 756696816,
            'sign2'             => '2484dc6b19d8e84ffdae3b2783dd27d8902f01cd325dc06d3e2251aa01aec37f',
            'amount'            => 16500,
            'uppTransactionId'  => "200326120742616710",
            'currency'          => 'CHF',
        ];
    }

    /**
     * @return array
     */
    private function mockedDatatransCancelResponse(): array
    {
        return [
            'merchantId'       => 1100020093,
            'status'           => StatusType::CANCEL,
            'amount'           => 4567,
            'uppTransactionId' => "200316101858862155",
            'currency'         => 'CHF',
        ];
    }

    /**
     * @return array
     */
    private function mockedDatatransErrorResponse(): array
    {
        return [
            'merchantId'       => 1100020093,
            'status'           => StatusType::ERROR,
            'amount'           => 4567,
            'uppTransactionId' => "200316101858862155",
            'currency'         => 'CHF',
            'errorCode'        => 4,
        ];
    }
}
