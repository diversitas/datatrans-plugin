<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Action\ConvertPaymentAction;
use Payum\Core\Model\Payment;
use Payum\Core\Model\PaymentInterface;
use Payum\Core\Request\Convert;
use Payum\Core\Request\Generic;
use Payum\Core\Security\TokenInterface;

class ConvertPaymentActionTest extends GenericActionTest
{
    protected $actionClass = ConvertPaymentAction::class;
    protected $requestClass = Convert::class;

    /**
     * @return array
     */
    public function provideSupportedRequests()
    {
        return [
            [new $this->requestClass(new Payment(), 'array')],
            [new $this->requestClass($this->createMock(PaymentInterface::class), 'array')],
            [new $this->requestClass(new Payment(), 'array', $this->createMock(TokenInterface::class))],
        ];
    }

    /**
     * @return array
     */
    public function provideNotSupportedRequests()
    {
        return [
            ['foo'],
            [['foo']],
            [new \stdClass()],
            [$this->getMockForAbstractClass(Generic::class, [[]])],
            [new $this->requestClass(new \stdClass(), 'array')],
            [new $this->requestClass(new Payment(), 'foobar')],
            [new $this->requestClass($this->createMock(PaymentInterface::class), 'foobar')],
        ];
    }

    /**
     * @test
     */
    public function shouldCorrectlyConvertOrderToDetailsAndSetItBack()
    {
        $order = new Payment();
        $order->setNumber('00000099');
        $order->setCurrencyCode('CHF');
        $order->setTotalAmount(1000);

        $action = new ConvertPaymentAction();
        $action->execute($convert = new Convert($order, 'array'));

        $details = $convert->getResult();

        $this->assertNotEmpty($details);

        $this->assertArrayHasKey('amount', $details);
        $this->assertEquals(1000, $details['amount']);

        $this->assertArrayHasKey('currency_code', $details);
        $this->assertEquals('CHF', $details['currency_code']);

        $this->assertArrayHasKey('refno', $details);
        $this->assertEquals('00000099', $details['refno']);
    }

    /**
     * @test
     */
    public function shouldNotOverwriteAlreadySetExtraDetails()
    {
        $order = new Payment();
        $order->setNumber('00000099');
        $order->setCurrencyCode('CHF');
        $order->setTotalAmount(1000);
        $order->setDetails(
            [
                'foo' => 'bar',
            ]
        );

        $action = new ConvertPaymentAction();
        $action->execute($convert = new Convert($order, 'array'));
        $details = $convert->getResult();

        $this->assertNotEmpty($details);
        $this->assertArrayHasKey('foo', $details);
        $this->assertEquals('bar', $details['foo']);
    }
}
