<?php

declare(strict_types=1);

namespace Tests\Diversitas\SyliusDatatransPlugin\PHPUnit\Action;

use Diversitas\SyliusDatatransPlugin\Action\CaptureAction;
use Diversitas\SyliusDatatransPlugin\Action\StatusAction;
use Diversitas\SyliusDatatransPlugin\Enum\StatusType;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\Request\GetHumanStatus;

class StatusActionTest extends GenericActionTest
{
    protected $actionClass = StatusAction::class;
    protected $requestClass = GetHumanStatus::class;

    /**
     * @test
     */
    public function shouldImplementGatewayAwareInterface()
    {
        $reflectionClass = new \ReflectionClass(CaptureAction::class);

        $this->assertTrue($reflectionClass->implementsInterface(GatewayAwareInterface::class));
    }

    /**
     * @test
     */
    public function shouldMarkNewIfHasNoStatus()
    {
        /** @var StatusAction $action */
        $action = new StatusAction;

        $action->execute($status = new GetHumanStatus(['amount' => 10000]));
        $this->assertTrue($status->isNew());
    }

    /**
     * @test
     */
    public function shouldMarkCapturedIfHasStatusSuccess()
    {
        $action = new StatusAction;

        $action->execute(
            $status = new GetHumanStatus(
                [
                    'status' => StatusType::SUCCESS,
                    'sign2'  => 'a5b787e4ab28435a3471f7da003078b60912efbf5fcd89b2050b065adc7d9479',
                ]
            )
        );

        $this->assertTrue($status->isCaptured());
    }

    /**
     * @test
     */
    public function shouldMarkCanceledIfHasCanceledStatus()
    {
        $action = new StatusAction;

        $action->execute(
            $status = new GetHumanStatus(
                [
                    'status' => StatusType::CANCEL
                ]
            )
        );

        $this->assertTrue($status->isCanceled());
    }

    /**
     * @test
     */
    public function shouldMarkFailedIfHasErrorStatus()
    {
        $action = new StatusAction;

        $action->execute(
            $status = new GetHumanStatus(
                [
                    'status'    => StatusType::ERROR,
                    'errorCode' => 2021,
                ]
            )
        );

        $this->assertTrue($status->isFailed());
    }

    /**
     * @test
     */
    public function shouldMarkUnknownIfStatusIsNotSupported()
    {
        $action = new StatusAction;

        $action->execute(
            $status = new GetHumanStatus(
                [
                    'status' => 'unknown-status-provided',
                ]
            )
        );

        $this->assertTrue($status->isUnknown());
    }
}
