## Overview

This plugin allows you to integrate Datatrans payment with Sylius platform app.

## Installation

```bash
composer require diversitas/datatrans-plugin
```

Add this to your composer.json:
```git
"repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:diversitas/datatrans-plugin.git"
        }
    ]
```

Add plugin dependencies in /config/bundles.php:

```php
$bundles = [
    Diversitas\SyliusDatatransPlugin\DiversitasSyliusDatatransPlugin::class => ['all' => true],
];
```

Import configuration to /config/packages/_sylius.yaml"

```yaml
imports:
    ...
    
    - { resource: "@DiversitasSyliusDatatransPlugin/Resources/config/config.yml" }
```
Add routes config for WebHookController to /config/routes.yaml

```yaml
diversitas_shop_api_datatrans_webhook:
    resource: "@DiversitasSyliusDatatransPlugin/Resources/config/routing.yml"
```